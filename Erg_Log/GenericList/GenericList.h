#ifndef _GENERIC_LIST_H_
#define _GENERIC_LIST_H_

typedef struct Node {
    void* value;
    struct Node* next;
} Node;

typedef struct List {
    Node* head;
} List;

typedef void ForEachFuncPtr(void* _value);
typedef void MapFuncPtr(void* res, const void* _value);
typedef void ReduceFuncPtr(void* total, const void* _value);

List* ListCreate(void);
void ListDestroy(List* list);
void ListInsert(List* list, const void* _value);
void ListForEach(List* list, ForEachFuncPtr foreach);
void ListMap(List* list, List* resList, MapFuncPtr map);
void ListReduce(List* list, void* total, ReduceFuncPtr reduce);

void GenericListApp();

#endif
