#include "fsm.h"
#include <assert.h>
#include <stdbool.h>    //Defines true, false keywords
#include <stdio.h>
#include <string.h>

//Populate Dispatch table with function pointers
StateCallback action[] = {
    StartCallback,
    ExploreCallback,
    AttackCallback,
    FleeCallback
    //Don't need to specify size, it is implicitly 4
};

State StartCallback(Input input) {
    return EXPLORE;
}
State ExploreCallback(Input input) {
    switch (input) {
        case SEE_PLAYER: return ATTACK; break;
        case NO_PLAYER:
        case LOW_HEALTH:
        case HIGH_HEALTH: return EXPLORE; break;
        default: assert(false); break;    //assert(false) causes program crash, we should never hit a default switch-case
    }
}
State AttackCallback(Input input) {
    switch (input) {
        case NO_PLAYER: return EXPLORE; break;
        case LOW_HEALTH: return FLEE; break;
        case SEE_PLAYER:
        case HIGH_HEALTH: return ATTACK; break;
        default: assert(false); break;
    }
}
State FleeCallback(Input input) {
    switch (input) {
        case HIGH_HEALTH: return EXPLORE; break;
        case SEE_PLAYER:
        case NO_PLAYER:
        case LOW_HEALTH: return FLEE; break;
        default: assert(false); break;
    }
}

static Input ToInput(const char c) {
    switch (c) {
        case 'p': return SEE_PLAYER; break;
        case 'n': return NO_PLAYER; break;
        case 'h': return HIGH_HEALTH; break;
        case 'l': return LOW_HEALTH; break;
        default: assert(false); break;
    }
}

static void ToString(State state, char* str) {
    switch (state) {
        case START: strcpy(str, "START"); break;
        case EXPLORE: strcpy(str, "EXPLORE"); break;
        case ATTACK: strcpy(str, "ATTACK"); break;
        case FLEE: strcpy(str, "FLEE"); break;
        case STATE_MAX:
        default: assert(false); break;
    }
}

static void Print(State state) {
    char str[32] = { 0 };
    ToString(state, str);
    printf("%s\n", str);
}

void FSMApp() {
    State state = START;
    action[state](INPUT_START);    //after statement, state == EXPLORE
    Print(state);

    int c = getchar();
    while (c != EOF) {
        if (c == 'q') {    //stdin: type 'q' button to quit
            break;
        } else if (c == '\n') {    //stdin: ignore 'enter' button
            c = getchar();
            continue;
        }
        Input input = ToInput(c);
        state = action[state](input);
        Print(state);
        c = getchar();
    }
}
