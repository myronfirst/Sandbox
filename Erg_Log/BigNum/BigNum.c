#include "BigNum.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX(x, y) ((x) > (y) ? (x) : (y))
#define MIN(x, y) ((x) < (y) ? (x) : (y))

static void AllocDigits(BigNum* val, const char* digits) {
    val->size = strlen(digits);
    val->digits = calloc(val->size, sizeof(unsigned char));
    for (size_t i = 0; i < val->size; ++i) // store from left -> right, least important digit -> most important digit
        val->digits[val->size - 1 - i] = digits[i] - '0';
}
static void AllocSize(BigNum* val, size_t size) {
    val->size = size;
    val->digits = calloc(val->size, sizeof(unsigned char));
}
static BigNum MultiplyDigit(BigNum val, unsigned char digit) {
    BigNum res;
    AllocSize(&res, val.size + 1);
    unsigned char kratoumeno = 0;
    for (size_t i = 0; i < val.size; ++i) {
        unsigned char _val = digit * val.digits[i] + kratoumeno;
        unsigned char mod = _val % 10;
        kratoumeno = _val / 10;
        res.digits[i] += mod;
    }
    res.digits[res.size - 1] += kratoumeno;
    return res;
}
static BigNum ShiftLeft(BigNum val, int distance) {
    BigNum res;
    AllocSize(&res, val.size + distance);
    for (size_t i = 0; i < val.size; ++i) {
        res.digits[i + distance] = val.digits[i];
    }
    return res;
}
static BigNum TrimZeroes(BigNum val) {
    size_t endPos = 0;
    for (size_t i = 0; i < val.size; ++i) {
        if (val.digits[i] != 0) endPos = i;
    }
    size_t endSize = endPos + 1;
    BigNum res;
    AllocSize(&res, endSize);
    memcpy(res.digits, val.digits, endSize * sizeof(unsigned char));
    return res;
}

BigNum BigNumCreate(const char* digits) {
    BigNum val;
    AllocDigits(&val, digits);
    return val;
}
void BigNumDestroy(BigNum val) {
    val.size = 0;
    free(val.digits);
    val.digits = NULL;
}
BigNum BigNumAdd(BigNum x, BigNum y) {
    BigNum res;
    AllocSize(&res, MAX(x.size, y.size) + 1);
    unsigned char kratoumeno = 0;
    for (size_t i = 0; i < MAX(x.size, y.size); ++i) {
        unsigned char val = x.digits[i] + y.digits[i] + kratoumeno;
        unsigned char mod = val % 10;
        kratoumeno = val / 10;
        res.digits[i] += mod;
    }
    res.digits[res.size - 1] += kratoumeno;
    res = TrimZeroes(res);    // mem leak
    return res;
}
BigNum BigNumMultiply(BigNum x, BigNum y) {
    BigNum res;
    AllocSize(&res, x.size + y.size);
    BigNum* minNum = (MIN(x.size, y.size) == x.size ? &x : &y);
    BigNum* maxNum = (minNum == &x ? &y : &x);
    for (size_t i = 0; i < minNum->size; ++i) {
        BigNum temp0 = MultiplyDigit(*maxNum, minNum->digits[i]);    // mem leak
        BigNum temp1 = ShiftLeft(temp0, i);                          // mem leak
        res = BigNumAdd(res, temp1);                                 // mem leak
    }
    res = TrimZeroes(res);    // mem leak
    return res;
}

void BigNumPrint(BigNum val) {
    for (size_t i = 0; i < val.size; ++i)
        printf("%u", val.digits[val.size - 1 - i]);
    printf("\n");
}

void BigNumApp() {
    BigNum x;
    BigNum y;
    (void)x;
    (void)y;

    BigNum a = BigNumCreate("10");
    BigNum b = BigNumCreate("20");
    BigNum c = BigNumAdd(a, b);
    BigNum d = BigNumMultiply(a, b);

    BigNumPrint(a);
    BigNumPrint(b);
    BigNumPrint(c);
    BigNumPrint(d);

    BigNumDestroy(a);
    BigNumDestroy(b);
    BigNumDestroy(c);
    BigNumDestroy(d);
}
