#ifndef _BIG_NUM_H_
#define _BIG_NUM_H_
#include <stdlib.h>

// June2018 Erotisi C 3
// May2023 Erotisi B 4

typedef struct BigNum {
    size_t size;
    unsigned char* digits;
} BigNum;

BigNum BigNumCreate(const char* digits);
void BigNumDestroy(BigNum val);
BigNum BigNumAdd(BigNum x, BigNum y);
BigNum BigNumMultiply(BigNum x, BigNum y);
void BigNumPrint(BigNum val);

void BigNumApp();

#endif
