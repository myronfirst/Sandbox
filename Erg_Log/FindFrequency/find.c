#include "find.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int find(char* s, char c, int* p) {
    if (s == NULL || p == NULL) return 0;
    int count = 0;    // count of c
    for (int i = 0; s[i] != '\0'; ++i) {
        if (s[i] == c) {
            p[count] = i;
            count++;
        }
    }
    return count;
}

static int findRec(char* s, char c, int* p, int i, int count) {
    if (s == NULL || p == NULL) return 0;
    if (s[i] == '\0') return count;
    if (s[i] == c) {
        p[count] = i;
        count++;
    }
    i++;
    return findRec(s, c, p, i, count);
}

static int findRecStatic(char* s, char c, int* p) {
    static int count = 0;    // count of c
    static int i = 0;        // s index
    if (s == NULL || p == NULL) return 0;
    if (s[i] == '\0') return count;
    if (s[i] == c) {
        p[count] = i;
        count++;
    }
    i++;
    return findRecStatic(s, c, p);
}

static void findPrints(int count, int* p) {
    const int sizeP = 32;
    char _p[32];
    for (int i = 0; i < sizeP; ++i)
        _p[i] = p[i] + '0';
    printf("count: %d, p: %s\n", count, _p);
}

void FindApp() {
    char s[] = "mamalakis";
    char c = 'a';
    int count = 0;
    int p[32] = { 0 };
    // const int countAns = 3;
    // const int pAns[32] = { 1, 3, 5 };
    memset(p, 0, sizeof(p));
    count = find(s, c, p);
    findPrints(count, p);
    memset(p, 0, sizeof(p));
    count = findRec(s, c, p, 0, 0);
    findPrints(count, p);
    memset(p, 0, sizeof(p));
    count = findRecStatic(s, c, p);
    findPrints(count, p);
}
