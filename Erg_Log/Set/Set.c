#include "Set.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Spring_2020, Thema C

// static SetPtr Copy(const SetPtr x) {
//     SetPtr ret = SetCreate(x->type_size, x->compare);
//     ret->size = x->size;
//     ret->data = calloc(1, ret->size * ret->type_size);
//     memcpy(ret->data, x->data, ret->size * ret->type_size);
//     return ret;
// }
static void Swap(void* a, void* b, size_t type_size) {
    assert(a && b);
    // assert(a != b);
    assert(type_size > 0);
    void* tmp = calloc(1, type_size);
    memcpy(tmp, a, type_size);
    memcpy(a, b, type_size);
    memcpy(b, tmp, type_size);
    free(tmp);
}
static void RemoveDupes(SetPtr x) {
    assert(x);
    if (x->size == 1) return;
    size_t j = x->size - 1;
    for (size_t i = 1; i < x->size; ++i) {
        void* addrA = ((char*)x->data) + ((i - 1) * x->type_size);
        void* addrB = ((char*)x->data) + (i * x->type_size);
        assert(x->compare(addrA, addrB) <= 0);
        if (x->compare(addrA, addrB) == 0) {
            void* addrEnd = ((char*)x->data) + (j * x->type_size);
            Swap(addrA, addrEnd, x->type_size);
            j -= 1;
        }
    }
    j += 1;
    if (j != x->size) {
        x->size = j;
        x->data = realloc(x->data, x->size * x->type_size);
    }
}

SetPtr SetCreate(size_t type_size, Compare compare) {
    assert(type_size > 0);
    assert(compare);
    SetPtr ret = calloc(1, sizeof(Set));
    ret->type_size = type_size;
    ret->compare = compare;
    ret->size = 0;
    ret->data = NULL;
    return ret;
}
void SetDestroy(SetPtr x) {
    free(x->data);
    free(x);
}
size_t SetSize(SetPtr x) {
    return x->size;
}
void SetInsert(SetPtr x, const void* val) {
    assert(x && val);
    x->size += 1;
    x->data = realloc(x->data, x->size * x->type_size);
    void* addr = ((char*)x->data) + (x->type_size * (x->size - 1));
    memcpy(addr, val, x->type_size);
    qsort(x->data, x->size, x->type_size, x->compare);
    RemoveDupes(x);
}
void SetRemove(SetPtr x, const void* val) {
    assert(x && val);
    for (size_t i = 0; i < x->size; ++i) {
        void* addr = ((char*)x->data) + (i * x->type_size);
        if (memcmp(addr, val, x->type_size) == 0) {
            void* addrEnd = ((char*)x->data) + ((x->size - 1) * x->type_size);
            Swap(addr, addrEnd, x->type_size);
            x->size -= 1;
            x->data = realloc(x->data, x->size * x->type_size);
            qsort(x->data, x->size, x->type_size, x->compare);
            break;
        }
    }
}
SetPtr SetUnion(SetPtr x, SetPtr y) {
    assert(x && y);
    assert(x->type_size == y->type_size);
    assert(x->compare == y->compare);
    SetPtr ret = SetCreate(x->type_size, x->compare);
    ret->size = x->size + y->size;
    ret->data = calloc(ret->size, ret->type_size);
    void* addrA = ret->data;
    void* addrB = ((char*)ret->data) + (x->size * ret->type_size);
    memcpy(addrA, x->data, x->size * x->type_size);
    memcpy(addrB, y->data, y->size * y->type_size);
    qsort(ret->data, ret->size, ret->type_size, ret->compare);
    return ret;
}
SetPtr SetIntersection(SetPtr x, SetPtr y) {
    assert(x && y);
    assert(x->type_size == y->type_size);
    assert(x->compare == y->compare);
    SetPtr ret = SetCreate(x->type_size, x->compare);
    for (size_t i = 0; i < y->size; ++i) {
        void* addr = ((char*)y->data) + (i * y->type_size);
        if (SetFind(x, addr)) SetInsert(ret, addr);
    }
    return ret;
}
int SetFind(SetPtr x, const void* val) {
    assert(x && val);
    for (size_t i = 0; i < x->size; ++i) {
        void* addr = ((char*)x->data) + (i * x->type_size);
        if (memcmp(addr, val, x->type_size) == 0) return 1;
    }
    return 0;
}
void SetPrint(SetPtr x, PrintFunc func) {
    assert(x && func);
    printf("type size: %ld\n", x->type_size);
    printf("size: %ld\n", x->size);
    for (size_t i = 0; i < x->size; ++i) {
        void* addr = ((char*)x->data) + (i * x->type_size);
        func(addr);
    }
    printf("\n");
}

static int CompareChars(const void* a, const void* b) {
    char _a = *(char*)a;
    char _b = *(char*)b;
    if (_a < _b) return -1;
    if (_a > _b) return 1;
    return 0;
}
static void PrintChar(const void* val) {
    const char _val = *(const char*)val;
    printf("%c", _val);
}
static void TestChar() {
    SetPtr a = SetCreate(sizeof(char), CompareChars);
    SetPtr b = SetCreate(sizeof(char), CompareChars);

    char ca = 'a';
    char cb = 'b';
    char ce = 'e';
    char cg = 'g';
    char cz = 'z';
    SetInsert(a, (const void*)&ca);
    SetInsert(a, (const void*)&cg);
    SetInsert(a, (const void*)&ce);
    SetInsert(b, (const void*)&cb);

    SetRemove(a, (const void*)&ca);
    SetRemove(b, (const void*)&cb);

    SetInsert(b, (const void*)&cz);
    SetPtr c = SetUnion(a, b);
    SetInsert(b, (const void*)&ce);
    SetPtr d = SetIntersection(a, b);

    int found = SetFind(a, (const void*)&ca);
    printf("found %d\n", found);
    found = SetFind(d, (const void*)&ce);
    printf("found %d\n", found);

    printf("a size %ld\n", SetSize(a));
    printf("b size %ld\n", SetSize(b));

    printf("set a\n");
    SetPrint(a, PrintChar);
    printf("set b\n");
    SetPrint(b, PrintChar);
    printf("set c\n");
    SetPrint(c, PrintChar);
    printf("set d\n");
    SetPrint(d, PrintChar);

    SetDestroy(a);
    SetDestroy(b);
    SetDestroy(c);
    SetDestroy(d);
}

#define SIZE 5
static int CompareFloats(const void* a, const void* b) {
    const float* _a = (const float*)a;
    const float* _b = (const float*)b;
    if (_a[0] < _b[0]) return -1;
    if (_a[0] > _b[0]) return 1;
    return 0;
}
static void PrintFloats(const void* val) {
    const float* _val = (const float*)val;
    for (size_t i = 0; i < SIZE; ++i)
        printf("%f ", _val[i]);
    printf("| ");
}
static void TestFloatArray() {
    float arr0[SIZE] = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f };
    float arr1[SIZE] = { 6.0f, 7.0f, 8.0f, 9.0f, 10.0f };
    float arr2[SIZE] = { 1.0f, 3.0f, 5.0f, 7.0f, 9.0f };
    float arr3[SIZE] = { 2.0f, 4.0f, 6.0f, 8.0f, 10.0f };

    SetPtr a = SetCreate(SIZE * sizeof(float), CompareFloats);
    SetPtr b = SetCreate(SIZE * sizeof(float), CompareFloats);

    SetInsert(a, arr0);
    SetInsert(b, arr1);

    SetRemove(a, arr0);
    SetInsert(a, arr2);

    SetPtr c = SetUnion(a, b);

    SetInsert(a, arr3);
    SetInsert(b, arr3);
    SetPtr d = SetIntersection(a, b);

    printf("set a\n");
    SetPrint(a, PrintFloats);
    printf("set b\n");
    SetPrint(b, PrintFloats);
    printf("set c\n");
    SetPrint(c, PrintFloats);
    printf("set d\n");
    SetPrint(d, PrintFloats);

    SetDestroy(a);
    SetDestroy(b);
    SetDestroy(c);
    SetDestroy(d);
}
void SetApp() {
    // SetPtr _x;
    // SetPtr _y;

    // TestChar();
    TestFloatArray();
}
