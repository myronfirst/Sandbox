#ifndef _SET_H_
#define _SET_H_
#include <stdlib.h>

// Spring_2020, Thema C

typedef int (*Compare)(const void* a, const void* b);
typedef struct Set {
    size_t type_size;
    Compare compare;
    size_t size;
    void* data;
} Set;
typedef Set* SetPtr;

SetPtr SetCreate(size_t type_size, Compare compare);
void SetDestroy(SetPtr x);
size_t SetSize(SetPtr x);
void SetInsert(SetPtr x, const void* val);
void SetRemove(SetPtr x, const void* val);
SetPtr SetUnion(SetPtr x, SetPtr y);
SetPtr SetIntersection(SetPtr x, SetPtr y);
int SetFind(SetPtr x, const void* val);

typedef void (*PrintFunc)(const void* val);
void SetPrint(SetPtr x, PrintFunc func);

void SetApp();

#endif
