#include <stdio.h>

typedef void (*InitEltFunc)(void* elt);
void Init2d(void* arr, size_t nrows, size_t ncols, size_t size, InitEltFunc InitElt) {
    char* _arr = (char*)arr;
    for (size_t i = 0u; i < nrows; ++i)
        for (size_t j = 0u; j < ncols; ++j) {
            InitElt((void*)_arr);
            _arr += size;
        }
}

void InitSeq(void* elt) {
    static int num = 0;
    int* _elt = (int*)elt;
    *_elt = num;
    ++num;
}

struct S {
    char* s;
    int v;
};
void InitStructS(void* elt) {
    static int num = 0;
    struct S* _elt = (struct S*)elt;
    _elt->s = NULL;
    _elt->v = num;
    ++num;
}

struct Vec3 {
    unsigned x;
    char y;
    char* z;
};
void InitStructVec(void* elt) {
    struct Vec3* _elt = (struct Vec3*)elt;
    _elt->x = 1u;
    _elt->y = '2';
    _elt->z = "333";
}

#define A_ROWS 5
#define A_COLS 10
#define B_ROWS 15
#define B_COLS 9
#define C_ROWS 2
#define C_COLS 3
int main() {
    int A[A_ROWS][A_COLS];
    struct S B[B_ROWS][B_COLS];
    struct Vec3 C[C_ROWS][C_COLS];

    Init2d(A, A_ROWS, A_COLS, sizeof(int), InitSeq);
    Init2d(B, B_ROWS, B_COLS, sizeof(struct S), InitStructS);
    Init2d(C, C_ROWS, C_COLS, sizeof(struct Vec3), InitStructVec);

    printf("A: ");
    for (size_t i = 0u; i < A_ROWS; ++i)
        for (size_t j = 0u; j < A_COLS; ++j)
            printf("%d ", A[i][j]);
    printf("\n");
    printf("B: ");
    for (size_t i = 0u; i < B_ROWS; ++i)
        for (size_t j = 0u; j < B_COLS; ++j)
            printf("(%s, %d) ", B[i][j].s, B[i][j].v);
    printf("\n");
    printf("C: ");
    for (size_t i = 0u; i < C_ROWS; ++i)
        for (size_t j = 0u; j < C_COLS; ++j)
            printf("(%u, %c, %s) ", C[i][j].x, C[i][j].y, C[i][j].z);
    printf("\n");

    return 0;
}
