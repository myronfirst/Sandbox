#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Interface
typedef struct Kvs Kvs;
typedef struct Node Node;
typedef struct GenericValue GenericValue;
struct GenericValue {
    void* data;
    size_t size;
};
struct Node {
    char* key;
    GenericValue value;
    struct Node* next;
};
struct Kvs {
    Node* head;
};

Kvs* KvsNew(void);
void KvsFree(Kvs* kvs);
size_t KvsGetLength(const Kvs* kvs);
int KvsPut(Kvs* kvs, const char* key, GenericValue value);
int KvsRemove(Kvs* kvs, const char* key);
int KvsGet(Kvs* kvs, const char* key, GenericValue* retValue);
int KvsGetView(Kvs* kvs, const char* key, void** retValuePtr, size_t* retValueSize);

// Utilities
void* Malloc(size_t size) {
    void* p = malloc(size);
    if (!p) {
        printf("Out of memory\n");
        exit(EXIT_FAILURE);
    }
    return p;
}

char* CopyKey(const char* key) {
    char* p = Malloc((strlen(key) + 1) * sizeof(char));
    strcpy(p, key);
    return p;
}

GenericValue CopyValue(GenericValue v) {
    GenericValue copy;
    copy.data = Malloc(v.size);
    memcpy(copy.data, v.data, v.size);
    copy.size = v.size;
    return copy;
}

void CloneValue(GenericValue* dest, GenericValue src) {
    assert(dest);
    // dest memory is already allocated for us
    memcpy(dest->data, src.data, src.size);
    dest->size = src.size;
}

void FreeValue(GenericValue v) { free(v.data); }

void FreeNode(Node* n) {
    if (n == NULL) return;
    free(n->key);
    FreeValue(n->value);
    free(n);
}

Node* GetNode(Kvs* kvs, const char* key) {
    for (Node* curr = kvs->head; curr != NULL; curr = curr->next) {
        if (strcmp(curr->key, key) == 0) return curr;
    }
    return NULL;
}

Node* ListPut(Node* head, const char* key, GenericValue value) {
    assert(key);
    Node* n = Malloc(sizeof(Node));
    n->key = CopyKey(key);
    n->value = CopyValue(value);
    n->next = head;
    head = n;
    return head;
}

Node* ListRemove(Node* head, const char* key) {
    Node* prev = NULL;
    Node* curr;
    for (curr = head; curr != NULL; curr = curr->next) {
        if (strcmp(curr->key, key) == 0) {
            if (prev == NULL)
                head = curr->next;
            else
                prev->next = curr->next;
            FreeNode(curr);
            return head;
        }
        prev = curr;
    }
    assert(0);    // pre-condition: list contains key
}

// Implementation
Kvs* KvsNew(void) {
    Kvs* kvs = Malloc(1 * sizeof(Kvs));
    kvs->head = NULL;
    return kvs;
}
void KvsFree(Kvs* kvs) {
    assert(kvs);
    Node* prev = NULL;
    for (Node* curr = kvs->head; curr != NULL; curr = curr->next) {
        FreeNode(prev);
        prev = curr;
    }
    FreeNode(prev);
}
size_t KvsGetLength(const Kvs* kvs) {
    assert(kvs);
    size_t sz = 0U;
    for (Node* curr = kvs->head; curr != NULL; curr = curr->next)
        sz += 1;
    return sz;
}
int KvsPut(Kvs* kvs, const char* key, GenericValue value) {
    assert(kvs && key);
    Node* retNode = GetNode(kvs, key);
    if (retNode) return 0;
    kvs->head = ListPut(kvs->head, key, value);
    return 1;
}
int KvsRemove(Kvs* kvs, const char* key) {
    assert(kvs && key);
    Node* retNode = GetNode(kvs, key);
    if (!retNode) return 0;
    kvs->head = ListRemove(kvs->head, key);
    return 1;
}
int KvsGet(Kvs* kvs, const char* key, GenericValue* retValue) {
    assert(kvs && key && retValue);
    Node* retNode = GetNode(kvs, key);
    if (!retNode) return 0;
    CloneValue(retValue, retNode->value);
    return 1;
}
int KvsGetView(Kvs* kvs, const char* key, void** retValuePtr, size_t* retValueSize) {
    assert(kvs && key && retValuePtr && retValueSize);
    Node* retNode = GetNode(kvs, key);
    if (!retNode) return 0;
    *retValuePtr = retNode->value.data;
    *retValueSize = retNode->value.size;
    return 1;
}

// Test
int main() {
    int numArray[] = { 0, 1, 2, 3 };
    GenericValue val1 = { .data = &numArray[1], .size = sizeof(int) };
    char* dynamicStr = Malloc(128 * sizeof(char));
    size_t sz = (strlen("dynamicStr") + 1) * sizeof(char);
    memcpy(dynamicStr, "dynamicStr", sz);
    GenericValue val2 = { .data = dynamicStr, .size = sz };
    char retArray[256] = { 0 };
    GenericValue retVal = { .data = retArray, .size = 0U };
    void* retValPtr = NULL;
    size_t retValSize = 0U;
    int ok = 0;
    {
        Kvs* kvs = KvsNew();
        assert(KvsGetLength(kvs) == 0);

        KvsPut(kvs, "1", val1);
        assert(KvsGetLength(kvs) == 1);

        ok = KvsGet(kvs, "1", &retVal);
        assert(ok);
        ok = KvsGet(kvs, "-1", &retVal);
        assert(!ok);

        ok = KvsGetView(kvs, "1", &retValPtr, &retValSize);
        assert(ok);
        ok = KvsGetView(kvs, "-1", &retValPtr, &retValSize);
        assert(!ok);

        ok = KvsRemove(kvs, "1");
        assert(ok);
        ok = KvsRemove(kvs, "-1");
        assert(!ok);

        KvsFree(kvs);
    }
    {
        Kvs* kvs = KvsNew();
        assert(KvsGetLength(kvs) == 0);

        KvsPut(kvs, "aaa", val2);
        assert(KvsGetLength(kvs) == 1);

        ok = KvsGet(kvs, "aaa", &retVal);
        assert(ok);
        ok = KvsGet(kvs, "bbbbb", &retVal);
        assert(!ok);

        ok = KvsGetView(kvs, "aaa", &retValPtr, &retValSize);
        assert(ok);
        ok = KvsGetView(kvs, "bbbbb", &retValPtr, &retValSize);
        assert(!ok);

        ok = KvsRemove(kvs, "aaa");
        assert(ok);
        ok = KvsRemove(kvs, "bbbbb");
        assert(!ok);

        KvsFree(kvs);
    }
    free(dynamicStr);
}
