#include <assert.h>
#include <stdio.h>
#include "MyString.h"

size_t ms_length(const char* s) {
    assert(s);
    const char* p = s;
    while (*p) ++p;
    return p - s;
}
char* ms_copy(char* dest, const char* src) {
    assert(dest && src);
    return ms_ncopy(dest, src, ms_length(src) + 1);
}
char* ms_ncopy(char* dest, const char* src, size_t n) {
    assert(dest && src);
    const char* srcPtr = src;
    char* destPtr;
    for (destPtr = dest; (destPtr < (dest + n)) && (*srcPtr != '\0'); ++destPtr, ++srcPtr)
        *destPtr = *srcPtr;
    for (; destPtr < (dest + n); ++destPtr)
        *destPtr = '\0';
    return dest;
}
char* ms_concat(char* dest, const char* src) {
    assert(dest && src);
    return ms_nconcat(dest, src, ms_length(src));
}
char* ms_nconcat(char* dest, const char* src, size_t n) {
    assert(dest && src);
    const char* srcPtr = src;
    const char* destPtrEnd = dest + ms_length(dest) + n;
    char* destPtr;
    for (destPtr = dest + ms_length(dest); (destPtr < destPtrEnd) && (*srcPtr != '\0'); ++destPtr, ++srcPtr)
        *destPtr = *srcPtr;
    *destPtr = '\0';
    return dest;
}
int ms_compare(const char* s1, const char* s2) {
    assert(s1 && s2);
    const size_t l1 = ms_length(s1);
    const size_t l2 = ms_length(s2);
    return ms_ncompare(s1, s2, l1 < l2 ? l1 : l2);
}
int ms_ncompare(const char* s1, const char* s2, size_t n) {
    assert(s1 && s2);
    const char* p1 = s1;
    const char* p2 = s2;
    while ((p1 < s1 + n) && (*p1 != '\0') && (*p1 == *p2)) {
        ++p1;
        ++p2;
    }
    return (const unsigned char)*p1 - (const unsigned char)*p2;
}
static int compare_needle_to_window(const char* haystackPtr, const char* needlePtr) {
    while (*haystackPtr && *needlePtr) {
        if (*haystackPtr != *needlePtr) return 0;
        ++haystackPtr;
        ++needlePtr;
    }
    return *needlePtr == '\0';
}
char* ms_search(const char* haystack, const char* needle) {
    assert(haystack && needle);
    if (ms_length(needle) == 0) return (char*)haystack;
    while (*haystack != '\0') {
        if ((*haystack == *needle) && compare_needle_to_window(haystack, needle)) return (char*)haystack;
        ++haystack;
    }
    return NULL;
}

int main() {
    char str1[] = "the white fox jumped over the lazy rabbit";
    char str2[] = "fox";
    char str3[] = "rabbit";
    char str4[] = "wolf";
    char str5[128] = "join";
    char empty[8] = { '\0' };
    printf("'' len %lu\n", ms_length(""));
    printf("fox len %lu\n", ms_length(str2));
    printf("'' <- '' %s\n", ms_copy(empty, ""));
    printf("rabbit <- wolf %s\n", ms_copy(str3, str4));
    printf("rabbit <- rabb %s\n", ms_ncopy(str3, "rabbit", 4));
    printf("'' + '' %s\n", ms_concat(empty, ""));
    printf("join + fox %s\n", ms_concat(str5, str2));
    printf("joinfox + join %s\n", ms_nconcat(str5, str5, 4));
    printf("'' == '' %d\n", ms_compare(empty, ""));
    printf("fox < foxx %d\n", ms_compare(str2, "foxx"));
    printf("fox == fox %d\n", ms_compare(str2, "fox"));
    printf("fox > fo %d\n", ms_compare(str2, "fo"));
    printf("fox == fox %d\n", ms_ncompare(str2, "foxx", 2));
    printf("'' search '' %s\n", ms_search(empty, ""));
    printf("str1 search fox %s\n", ms_search(str1, str2));
    printf("str1 search dog %s\n", ms_search(str1, "dog"));
}
