#include <assert.h>
#include <stdio.h>
#include "MyString.h"

size_t ms_length(const char* s) {
    assert(s);
    size_t i = 0u;
    while (s[i] != '\0') ++i;
    return i;
}
char* ms_copy(char* dest, const char* src) {
    assert(dest && src);
    return ms_ncopy(dest, src, ms_length(src) + 1);
}
char* ms_ncopy(char* dest, const char* src, size_t n) {
    assert(dest && src);
    size_t i;
    for (i = 0; i < n && src[i] != '\0'; ++i)
        dest[i] = src[i];
    for (; i < n; ++i)
        dest[i] = '\0';
    return dest;
}
char* ms_concat(char* dest, const char* src) {
    assert(dest && src);
    return ms_nconcat(dest, src, ms_length(src));
}
char* ms_nconcat(char* dest, const char* src, size_t n) {
    assert(dest && src);
    const size_t dest_len = ms_length(dest);
    size_t i;
    for (i = 0; i < n && src[i] != '\0'; ++i)
        dest[dest_len + i] = src[i];
    dest[dest_len + i] = '\0';
    return dest;
}
int ms_compare(const char* s1, const char* s2) {
    assert(s1 && s2);
    const size_t l1 = ms_length(s1);
    const size_t l2 = ms_length(s2);
    return ms_ncompare(s1, s2, l1 < l2 ? l1 : l2);
}
int ms_ncompare(const char* s1, const char* s2, size_t n) {
    assert(s1 && s2);
    size_t i = 0;
    while ((i < n) && (s1[i] != '\0') && (s2[i] != '\0') && (s1[i] == s2[i])) ++i;
    return (const unsigned char)s1[i] - (const unsigned char)s2[i];
}
static int compare_needle_to_window(const char* haystack, const char* needle, const size_t haystack_index) {
    size_t i = 0;
    while (haystack[haystack_index + i] && needle[i]) {
        if (haystack[haystack_index + i] != needle[i]) return 0;
        ++i;
    }
    return needle[i] == '\0';
}
char* ms_search(const char* haystack, const char* needle) {
    assert(haystack && needle);
    if (ms_length(needle) == 0) return (char*)haystack;
    size_t i = 0;
    while (haystack[i] != '\0') {
        if ((haystack[i] == needle[0]) && compare_needle_to_window(haystack, needle, i)) return (char*)&haystack[i];
        ++i;
    }
    return NULL;
}

int main() {
    char str1[] = "the white fox jumped over the lazy rabbit";
    char str2[] = "fox";
    char str3[] = "rabbit";
    char str4[] = "wolf";
    char str5[128] = "join";
    char empty[8] = { '\0' };
    printf("'' len %lu\n", ms_length(""));
    printf("fox len %lu\n", ms_length(str2));
    printf("'' <- '' %s\n", ms_copy(empty, ""));
    printf("rabbit <- wolf %s\n", ms_copy(str3, str4));
    printf("rabbit <- rabb %s\n", ms_ncopy(str3, "rabbit", 4));
    printf("'' + '' %s\n", ms_concat(empty, ""));
    printf("join + fox %s\n", ms_concat(str5, str2));
    printf("joinfox + join %s\n", ms_nconcat(str5, str5, 4));
    printf("'' == '' %d\n", ms_compare(empty, ""));
    printf("fox < foxx %d\n", ms_compare(str2, "foxx"));
    printf("fox == fox %d\n", ms_compare(str2, "fox"));
    printf("fox > fo %d\n", ms_compare(str2, "fo"));
    printf("fox == fox %d\n", ms_ncompare(str2, "foxx", 2));
    printf("'' search '' %s\n", ms_search(empty, ""));
    printf("str1 search fox %s\n", ms_search(str1, str2));
    printf("str1 search dog %s\n", ms_search(str1, "dog"));
    return 0;
}
