We cannot use assert to determine the available memory of our destination array in ms_copy, ms_concat.
The available bytes are not passed as a function parameter.
Furthermore, there is no C function/operator which determines the length of the valid address space to which a pointer points to.
C arrays and C pointers are translated to C pointers when used as a function parameter. So both of the following examples are the same
void f(const char[] ptr);
void f(const char* ptr)
param is always translated to a pointer.

We do not need to use assert to determine whether ms_length returns a non negative size. The type system helps us in this regard since the return type size_t is an unsigned long type, meaning it cannot represent negative values.

ms_compare is based on the implemention of strcmp().
strcmp() compares the strings lexicographicaly using the unsigned char type for comparison.
For this reason, in our implementation we need to cast to unsigned char the elements, of type char, of our strings, before comparing them.
