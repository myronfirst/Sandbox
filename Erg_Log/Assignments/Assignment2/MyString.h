#ifndef _MY_STRING_H_
#define _MY_STRING_H_
#include <stddef.h>

size_t ms_length(const char* s);
char* ms_copy(char* dest, const char* src);
char* ms_ncopy(char* dest, const char* src, size_t n);
char* ms_concat(char* dest, const char* src);
char* ms_nconcat(char* dest, const char* src, size_t n);
int ms_compare(const char* s1, const char* s2);
int ms_ncompare(const char* s1, const char* s2, size_t n);
char* ms_search(const char* haystack, const char* needle);

#endif
