#ifndef _UTILS_H_
#define _UTILS_H_

#include <stddef.h>

struct Node {
    const char* key;
    const void* value;
    struct Node* next;
};
typedef struct Node Node;

struct List {
    Node* head;
    size_t length;
};
typedef struct List List;

void* Malloc(size_t size);
const char* CopyKey(const char* key);
void FreeNode(Node* n);
void ListFree(List list);
void* ListGet(List list, const char* key);
List ListPut(List list, const char* key, const void* value);
List ListRemove(List list, const char* key);
void ListApply(List list, void (*Apply)(const char* key, void* value, void* extra), const void* extra);

#endif
