#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "SymTable.h"
#include "SymTableTest.h"
#include "Utils.h"

size_t ReallocsNum;

static void Add(const char* key, void* value, void* extra) {
    (void)key;
    ((int*)value)[0] += ((int*)extra)[0];
}
static void SimpleTest() {
    SymTable* t;
    t = SymTableNew();
    SymTableFree(t);

    t = SymTableNew();
    assert(SymTableGetLength(t) == 0);
    int one = 1;
    assert(SymTablePut(t, "a", &one));
    assert(SymTableGetLength(t) == 1);
    assert(SymTableContains(t, "a"));
    assert(SymTableGet(t, "a") == &one);
    int extra = 10;
    SymTableMap(t, Add, &extra);
    assert(one == 11);
    SymTableFree(t);
}

#define ALPHABET_SIZE 26
static void Fill(const char* key, void* value, void* extra) {
    memset(value, ((char*)extra)[0], strlen(key));
}
static void PrintAlphabetPtr(char* alphabetPtr[]) {
    for (int i = 0; i < ALPHABET_SIZE; ++i) printf("%s\n", alphabetPtr[i]);
}
static void AlphabetTest() {
    // Each key and value contain part of the alphabet
    SymTable* t = SymTableNew();
    char* alphabetPtr[ALPHABET_SIZE] = { NULL };
    char alphabetState[ALPHABET_SIZE + 1] = { '\0' };
    for (int i = 0; i < ALPHABET_SIZE; ++i) {
        char letter = i + 'A';
        alphabetState[i] = letter;
        alphabetPtr[i] = malloc((strlen(alphabetState) + 1) * sizeof(char));
        assert(alphabetPtr[i]);
        strcpy(alphabetPtr[i], alphabetState);
        assert(SymTablePut(t, alphabetPtr[i], alphabetPtr[i]));
        assert(SymTableGetLength(t) == (unsigned int)(i + 1));
        assert(SymTableContains(t, alphabetPtr[i]));
    }
    for (int i = 0; i < ALPHABET_SIZE; ++i) {
        assert(SymTableGet(t, alphabetPtr[i]) == alphabetPtr[i]);
        assert(SymTablePut(t, alphabetPtr[i], alphabetPtr[i]) == 0);
        assert(SymTableRemove(t, alphabetPtr[i]) == 1);
        assert(SymTablePut(t, alphabetPtr[i], alphabetPtr[i]) == 1);
    }
    // PrintAlphabetPtr(alphabetPtr);
    char OneChar = '1';
    SymTableMap(t, Fill, &OneChar);
    // PrintAlphabetPtr(alphabetPtr);
    for (int i = 0; i < ALPHABET_SIZE; ++i) {
        for (int j = 0; j < i + 1; ++j) {
            assert(alphabetPtr[i][j] == '1');
        }
        free(alphabetPtr[i]);
    }
    SymTableFree(t);
}

#define KEYS_LEN (1 * 10000)
#define DATA_LEN 10
struct Value {
    int id;
    int data[DATA_LEN];
};
static void HeavyLoadTest() {
    char* keys[KEYS_LEN];
    struct Value values[KEYS_LEN];
    for (size_t i = 0; i < KEYS_LEN; ++i) {
        char buf[10];
        sprintf(buf, "%lu", i);
        keys[i] = Malloc(10 * sizeof(char));
        strcpy(keys[i], buf);
        values[i].id = i;
        for (size_t j = 0; j < DATA_LEN; ++j)
            values[i].data[j] = i;
    }
    ReallocsNum = 0;
    SymTable* t = SymTableNew();
    for (size_t i = 0; i < KEYS_LEN; ++i) {
        assert(SymTablePut(t, keys[i], &(values[i])));
        assert(SymTableGet(t, keys[i]) == (struct Value*)(&(values[i])));
        assert(SymTableContains(t, keys[i]));
        assert(SymTableGetLength(t) == (unsigned int)(i + 1));
    }
    for (size_t i = 0; i < KEYS_LEN; ++i) {
        const size_t ri = KEYS_LEN - (i + 1);
        assert(SymTableRemove(t, keys[ri]));
        assert(SymTableGet(t, keys[ri]) == NULL);
        assert(SymTableContains(t, keys[ri]) == 0);
        assert(SymTableGetLength(t) == (ri));
    }
    printf("HeavyLoadTest: Reallocation Times: %lu\n", ReallocsNum);

    for (size_t i = 0; i < KEYS_LEN; ++i)
        free(keys[i]);
    SymTableFree(t);
}

#define REALLOC_KEYS_LEN 32771
#define REALLOC_LOOP_LEN 100
static void ReallocationTest() {
    char* keys[REALLOC_KEYS_LEN];
    int values[REALLOC_KEYS_LEN];
    for (size_t i = 0; i < REALLOC_KEYS_LEN; ++i) {
        char buf[10];
        sprintf(buf, "%lu", i);
        keys[i] = Malloc(10 * sizeof(char));
        strcpy(keys[i], buf);
        values[i] = i;
    }
    SymTable* t = SymTableNew();
    for (size_t i = 0; i < REALLOC_KEYS_LEN; ++i)
        assert(SymTablePut(t, keys[i], &(values[i])));

    ReallocsNum = 0;
    for (size_t i = 0; i < REALLOC_LOOP_LEN; ++i) {
        const char* TestKey = "696969696969";
        int TestVal = 69;
        assert(SymTablePut(t, TestKey, &TestVal));
        assert(SymTableRemove(t, TestKey));
    }
    printf("ReallocationTest: Reallocation Times: %lu\n", ReallocsNum);

    for (size_t i = 0; i < REALLOC_KEYS_LEN; ++i)
        free(keys[i]);
    SymTableFree(t);
}

void SymTableTest() {
    SimpleTest();
    printf("SimpleTest End\n");
    AlphabetTest();
    printf("AlphabetTest End\n");
    HeavyLoadTest();
    printf("HeavyLoadTest End\n");
    ReallocationTest();
    printf("ReallocationTest End\n");
}
