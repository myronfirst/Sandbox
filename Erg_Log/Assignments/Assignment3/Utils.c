#include "Utils.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void* Malloc(size_t size) {
    void* p = malloc(size);
    if (!p) {
        printf("Out of memory\n");
        exit(EXIT_FAILURE);
    }
    return p;
}

const char* CopyKey(const char* key) {
    char* p = Malloc((strlen(key) + 1) * sizeof(char));
    strcpy(p, key);
    return p;
}

void FreeNode(Node* n) {
    if (n == NULL) return;
    free((void*)n->key);
    free(n);
}

void ListFree(List list) {
    Node* prev = NULL;
    for (Node* curr = list.head; curr != NULL; curr = curr->next) {
        FreeNode(prev);
        prev = curr;
    }
    FreeNode(prev);
    list.length = 0u;
}

void* ListGet(List list, const char* key) {
    for (Node* curr = list.head; curr != NULL; curr = curr->next) {
        if (strcmp(curr->key, key) == 0) return (void*)curr->value;
    }
    return NULL;
}

List ListPut(List list, const char* key, const void* value) {
    Node* n = Malloc(sizeof(Node));
    n->key = CopyKey(key);
    n->value = value;
    n->next = list.head;
    list.head = n;
    list.length += 1;
    return list;
}

List ListRemove(List list, const char* key) {
    Node* prev = NULL;
    Node* curr;
    for (curr = list.head; curr != NULL; curr = curr->next) {
        if (strcmp(curr->key, key) == 0) {
            if (prev == NULL)
                list.head = curr->next;
            else
                prev->next = curr->next;
            FreeNode(curr);
            list.length -= 1;
            return list;
        }
        prev = curr;
    }
    assert(0);    // pre-condition: list contains key
}
void ListApply(List list, void (*Apply)(const char* key, void* value, void* extra), const void* extra) {
    for (Node* curr = list.head; curr != NULL; curr = curr->next) {
        (*Apply)(curr->key, (void*)curr->value, (void*)extra);
    }
}
