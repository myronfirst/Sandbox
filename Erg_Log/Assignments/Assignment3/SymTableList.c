#include "SymTable.h"
#include "SymTableTest.h"
#include "Utils.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct SymTable {
    List impl;
};

SymTable* SymTableNew(void) {
    SymTable* table = Malloc(sizeof(SymTable));
    table->impl.head = NULL;
    table->impl.length = 0u;
    return table;
}

void SymTableFree(SymTable* table) {
    if (!table) return;
    ListFree(table->impl);
    free(table);
}

unsigned int SymTableGetLength(SymTable* table) {
    assert(table);
    return table->impl.length;
}

int SymTablePut(SymTable* table, const char* key, const void* value) {
    assert(table && key);
    if (SymTableContains(table, key)) return 0;
    table->impl = ListPut(table->impl, key, value);
    return 1;
}

int SymTableRemove(SymTable* table, const char* key) {
    assert(table && key);
    if (!SymTableContains(table, key)) return 0;
    table->impl = ListRemove(table->impl, key);
    return 1;
}

int SymTableContains(SymTable* table, const char* key) {
    assert(table && key);
    return (SymTableGet(table, key) != NULL);
}

void* SymTableGet(SymTable* table, const char* key) {
    assert(table && key);
    return ListGet(table->impl, key);
}

void SymTableMap(SymTable* table, void (*Apply)(const char* key, void* value, void* extra), const void* extra) {
    assert(table && Apply);
    ListApply(table->impl, Apply, extra);
}

int main() {
    SymTableTest();
    return 0;
}
