#include "SymTable.h"
#include "SymTableTest.h"
#include "Utils.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TABLE_SIZE_LEN 8
const size_t TableSize[TABLE_SIZE_LEN] = { 509u, 1021u, 2053u, 4093u, 8191u, 16381u, 32771u, 65521u };

#define HASH_MULTIPLIER 65599
static unsigned int Hash(const char* pcKey) {
    unsigned int uiHash = 0u;
    for (size_t ui = 0u; pcKey[ui] != '\0'; ++ui)
        uiHash = uiHash * HASH_MULTIPLIER + pcKey[ui];
    return uiHash;
}

struct HashTable {
    List* hashTable;
    size_t sizeIndex;
    size_t length;
};
typedef struct HashTable HashTable;

struct SymTable {
    HashTable impl;
};

static SymTable* NewFromSizeIndex(size_t sizeIndex) {
    SymTable* table = Malloc(sizeof(SymTable));
    table->impl.hashTable = Malloc(TableSize[sizeIndex] * sizeof(List));
    memset(table->impl.hashTable, 0, TableSize[sizeIndex] * sizeof(List));
    table->impl.sizeIndex = sizeIndex;
    table->impl.length = 0u;
    return table;
}

static void HashTableFree(HashTable impl) {
    for (size_t i = 0; i < TableSize[impl.sizeIndex]; ++i)
        ListFree(impl.hashTable[i]);
    free(impl.hashTable);
}

static void DirectPut(SymTable* table, const char* key, const void* value) {
    const size_t i = Hash(key) % TableSize[table->impl.sizeIndex];
    table->impl.hashTable[i] = ListPut(table->impl.hashTable[i], key, value);
    table->impl.length += 1;
}

static void DirectRemove(SymTable* table, const char* key) {
    const size_t i = Hash(key) % TableSize[table->impl.sizeIndex];
    table->impl.hashTable[i] = ListRemove(table->impl.hashTable[i], key);
    table->impl.length -= 1;
}

static void CheckReallocation(SymTable* table, int lengthDifference) {
    assert(table);
    if (lengthDifference == 0) return;
    if (lengthDifference > 0 && table->impl.sizeIndex == TABLE_SIZE_LEN - 1) return;
    if (lengthDifference < 0 && table->impl.sizeIndex == 0) return;
    size_t newLength = SymTableGetLength(table) + lengthDifference;
    size_t newSizeIndex;
    if (lengthDifference > 0 && newLength > TableSize[table->impl.sizeIndex])
        newSizeIndex = table->impl.sizeIndex + 1;
    else if (lengthDifference < 0 && newLength <= TableSize[table->impl.sizeIndex - 1])
        newSizeIndex = table->impl.sizeIndex - 1;
    else
        return;
    // Redistribute
    SymTable* newTable = NewFromSizeIndex(newSizeIndex);
    for (size_t i = 0; i < TableSize[table->impl.sizeIndex]; ++i)
        for (Node* curr = table->impl.hashTable[i].head; curr != NULL; curr = curr->next)
            DirectPut(newTable, curr->key, curr->value);

    HashTableFree(table->impl);
    table->impl = newTable->impl;
    free(newTable);
    ReallocsNum += 1;
}

SymTable* SymTableNew(void) {
    return NewFromSizeIndex(0u);
}

void SymTableFree(SymTable* table) {
    if (!table) return;
    HashTableFree(table->impl);
    free(table);
}

unsigned int SymTableGetLength(SymTable* table) {
    assert(table);
    return table->impl.length;
}

int SymTablePut(SymTable* table, const char* key, const void* value) {
    assert(table && key);
    if (SymTableContains(table, key)) return 0;
    CheckReallocation(table, 1);
    DirectPut(table, key, value);
    return 1;
}

int SymTableRemove(SymTable* table, const char* key) {
    assert(table && key);
    if (!SymTableContains(table, key)) return 0;
    CheckReallocation(table, -1);
    DirectRemove(table, key);
    return 1;
}

int SymTableContains(SymTable* table, const char* key) {
    assert(table && key);
    return (SymTableGet(table, key) != NULL);
}

void* SymTableGet(SymTable* table, const char* key) {
    assert(table && key);
    const size_t i = Hash(key) % TableSize[table->impl.sizeIndex];
    return ListGet(table->impl.hashTable[i], key);
}

void SymTableMap(SymTable* table, void (*Apply)(const char* key, void* value, void* extra), const void* extra) {
    assert(table && Apply);
    for (size_t i = 0; i < TableSize[table->impl.sizeIndex]; ++i)
        ListApply(table->impl.hashTable[i], Apply, extra);
}

int main() {
    SymTableTest();
    return 0;
}
