#ifndef _SYMTABLE_H_
#define _SYMTABLE_H_

typedef struct SymTable SymTable;

SymTable* SymTableNew(void);
void SymTableFree(SymTable* table);
unsigned int SymTableGetLength(SymTable* table);
int SymTablePut(SymTable* table, const char* key, const void* value);
int SymTableRemove(SymTable* table, const char* key);
int SymTableContains(SymTable* table, const char* key);
void* SymTableGet(SymTable* table, const char* key);
void SymTableMap(SymTable* table, void (*Apply)(const char* key, void* value, void* extra), const void* extra);

#endif
