#ifndef _VOP_H_
#define _VOP_H_
#include <stdlib.h>

// August_2018, Thema C

typedef void (*Praxi)(void* res, void* a, void* b);
void vop(void* a, void* b, void* c, size_t nmemb, size_t size, Praxi praxi);
void VOPApp();

#endif
