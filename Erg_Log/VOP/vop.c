#include "vop.h"
#include <stdio.h>
#include <stdlib.h>

// September2018, Thema C

void vop(void* a, void* b, void* c, size_t nmemb, size_t size, Praxi praxi) {
    char* _a = (char*)a;
    char* _b = (char*)b;
    char* _c = (char*)c;
    for (size_t i = 0; i < nmemb; ++i) {
        size_t offset = i * size;
        (*praxi)(_c + offset, _a + offset, _b + offset);
    }
}

static void prosthesiLong(void* res, void* a, void* b) {
    long* _res = (long*)res;    // void* -> long*
    long* _a = (long*)a;
    long* _b = (long*)b;
    *_res = *_a + *_b;
}
static void prosthesiInt(void* res, void* a, void* b) {
    int* _res = (int*)res;
    int* _a = (int*)a;
    int* _b = (int*)b;
    *_res = *_a + *_b;
}
static void afairesiInt(void* res, void* a, void* b) {
    int* _res = (int*)res;
    int* _a = (int*)a;
    int* _b = (int*)b;
    *_res = *_a - *_b;
}
static void prosthesiDouble(void* res, void* a, void* b) {
    double* _res = (double*)res;
    double* _a = (double*)a;
    double* _b = (double*)b;
    *_res = *_a + *_b;
}
static void afairesiDouble(void* res, void* a, void* b) {
    double* _res = (double*)res;
    double* _a = (double*)a;
    double* _b = (double*)b;
    *_res = *_a - *_b;
}

#define NMEMB 5
void VOPApp() {
    printf("Sizeof void* %ld\n", sizeof(void*));
    printf("Sizeof int* %ld\n", sizeof(int*));
    printf("Sizeof long* %ld\n", sizeof(long*));
    printf("Sizeof double* %ld\n", sizeof(double*));
    printf("Sizeof int %ld\n", sizeof(int));
    printf("Sizeof long %ld\n", sizeof(long));
    printf("Sizeof double %ld\n", sizeof(double));

    // long a[NMEMB] = { 1, 2, 3, 4, 5 };
    // long b[NMEMB] = { 1, 2, 3, 4, 5 };
    // long c[NMEMB] = { 0, 0, 0, 0, 0 };
    // size_t size = sizeof(long);
    // const char* formatStr = "%ld ";
    // Praxi praxi = prosthesiLong;

    // int a[NMEMB] = { 1, 2, 3, 4, 5 };
    // int b[NMEMB] = { 1, 2, 3, 4, 5 };
    // int c[NMEMB] = { 0, 0, 0, 0, 0 };
    // size_t size = sizeof(int);
    // const char* formatStr = "%d ";
    // Praxi praxi = prosthesiInt;
    // Praxi praxi = afairesiInt;

    double a[NMEMB] = { 1.0, 2.0, 3.0, 4.0, 5.0 };
    double b[NMEMB] = { 1.0, 2.0, 3.0, 4.0, 5.0 };
    double c[NMEMB] = { 0.0, 0.0, 0.0, 0.0, 0.0 };
    const char* formatStr = "%lf ";
    size_t size = sizeof(double);
    Praxi praxi = prosthesiDouble;
    // Praxi praxi = afairesiDouble;

    vop((void*)a, (void*)b, (void*)c, NMEMB, size, praxi);
    for (int i = 0; i < NMEMB; ++i)
        printf(formatStr, c[i]);
    printf("\n");
}
