#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/*
    Double Linked Queue
*/

struct Node {
  int data;
  struct Node* prev;
  struct Node* next;
};
typedef struct Node Node;

Node* Front = NULL;
Node* Back = NULL;

void Create();
void Destroy();
bool IsEmpty();
int GetSize();
int GetFront();
int GetBack();
void Enqueue(int data);
void Dequeue();

void Error(const char* msg) {
  printf("File: %s, Line: %d, %s\n", __FILE__, __LINE__, msg);
  assert(false);
  exit(EXIT_FAILURE);
}

bool IsEmpty() {
  assert((Front && Back) || (!Front && !Back));
  return !Front;
}
void Create() {
  assert((!Front && !Back));
  Front = Back = NULL;
}
void Destroy() {
  assert((Front && Back) || (!Front && !Back));
  if (IsEmpty()) return;
  Node* prev = Front;
  for (Node* curr = Front->next; curr != NULL; curr = curr->next) {
    free(prev);
    prev = curr;
  }
  free(prev);
  Front = Back = NULL;
}
int GetSize() {
  assert((Front && Back) || (!Front && !Back));
  int size = 0;
  for (Node* curr = Front; curr != NULL; curr = curr->next) ++size;
  return size;
}
int GetFront() {
  if (IsEmpty()) Error("Empty Queue");
  return Front->data;
}
int GetBack() {
  if (IsEmpty()) Error("Empty Queue");
  return Back->data;
}
void Enqueue(int data) {
  Node* n = calloc(1, sizeof(Node));
  n->data = data;
  if (IsEmpty())
    Front = n;
  else
    Back->next = n;
  n->prev = Back;
  Back = n;
}
void Dequeue() {
  if (IsEmpty()) Error("Empty Queue");
  Node* p = Front;
  Front = Front->next;
  if (Front == NULL) Back = NULL;
  free(p);
}

static void PrintQueue() {
  Node* curr;
  for (curr = Front; curr != NULL; curr = curr->next)
    printf("<- %d ->", curr->data);
  printf("NULL\n");
}

void Test() {
  Create();
  Destroy();

  Create();
  Enqueue(1);
  Dequeue(1);
  assert(IsEmpty());
  Destroy();

  Create();
  Enqueue(1);
  assert(GetFront() == 1);
  assert(GetBack() == 1);
  Enqueue(2);
  assert(GetFront() == 1);
  assert(GetBack() == 2);
  Enqueue(3);
  assert(GetFront() == 1);
  assert(GetBack() == 3);
  Dequeue();
  assert(GetFront() == 2);
  assert(GetBack() == 3);
  Dequeue();
  assert(GetFront() == 3);
  assert(GetBack() == 3);
  Dequeue();
  Destroy();

  Create();
  for (int i = 0; i < 100; ++i) {
    Enqueue(i);
    assert(GetBack() == i);
  }
  assert(GetSize() == 100);
  for (int i = 0; i < 100; ++i) {
    assert(GetFront() == i);
    Dequeue();
  }
  assert(GetSize() == 0);
  assert(IsEmpty());
  Destroy();
}
int main() {
  Test();
  return 0;
}
