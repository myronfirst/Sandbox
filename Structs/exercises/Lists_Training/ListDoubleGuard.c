#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/*
    Double Linked Circular List with Guard node
*/

struct Node {
  int data;
  struct Node* prev;
  struct Node* next;
};
typedef struct Node Node;

Node* Head = NULL;
Node* Guard = NULL;

bool IsEmpty();
void Create();
void Destroy();
int GetSize();
bool Find(int data);
void Insert(int data);
bool Remove(int data);

void Error(const char* msg) {
  printf("File: %s, Line: %d, %s\n", __FILE__, __LINE__, msg);
  assert(false);
  exit(EXIT_FAILURE);
}

bool IsEmpty() {
  assert(Head && Guard);
  return Head == Guard;
}
void Create() {
  assert(!Head && !Guard);
  Guard = calloc(1, sizeof(Node));
  Guard->data = -1;
  Guard->prev = Guard->next = Guard;
  Head = Guard;
}
void Destroy() {
  assert(Head && Guard);
  Node* prev = Head;
  for (Node* curr = Head->next; curr != Guard; curr = curr->next) {
    free(prev);
    prev = curr;
  }
  free(prev);
  Head = Guard = NULL;
}
int GetSize() {
  assert(Head && Guard);
  int size = 0;
  for (Node* curr = Head; curr != Guard; curr = curr->next) ++size;
  return size;
}
bool Find(int data) {
  assert(Head && Guard);
  Guard->data = data;
  Node* curr;
  for (curr = Head; curr->data != data; curr = curr->next)
    ;
  Guard->data = -1;
  return !(curr == Guard);
}
void Insert(int data) {
  assert(Head && Guard);
  Node* n = calloc(1, sizeof(Node));
  n->data = data;
  n->next = Head;
  n->prev = Guard;
  Guard->next = n;
  Head->prev = n;
  Head = n;
}
bool Remove(int data) {
  assert(Head && Guard);
  Guard->data = data;
  Node* curr;
  for (curr = Head; curr->data != data; curr = curr->next)
    ;
  Guard->data = -1;
  if (curr == Guard) return false;
  curr->prev->next = curr->next;
  curr->next->prev = curr->prev;
  if (Head == curr) Head = curr->next;
  free(curr);
  return true;
}

static void PrintList() {
  assert(Head && Guard);
  Node* curr;
  for (curr = Head; curr != Guard; curr = curr->next)
    printf("<- %d ->", curr->data);
  printf("<- Guard %d ->\n", Guard->data);
}

void Test() {
  Create();
  Destroy();

  Create();
  assert(Remove(1) == false);
  Destroy();

  Create();
  Insert(1);
  assert(Find(1));
  assert(Remove(1));
  Destroy();

  Create();
  assert(Find(2) == false);
  Insert(2);
  assert(Remove(2));
  Destroy();

  Create();
  for (int i = 0; i < 100; ++i) Insert(i);
  for (int i = 0; i < 100; ++i) assert(Find(i));
  assert(GetSize() == 100);
  for (int i = 0; i < 100; ++i) assert(Remove(i));

  Destroy();
}

int main() {
  Test();
  return 0;
}
