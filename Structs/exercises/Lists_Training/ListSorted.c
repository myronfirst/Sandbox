#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/*
    Sorted Linked List
*/

struct Node {
  int data;
  struct Node* next;
};
typedef struct Node Node;

Node* Head = NULL;

bool IsEmpty();
void Create();
void Destroy();
int GetSize();
bool Find(int data);
void Insert(int data);
bool Remove(int data);

static void PrintList() {
  for (Node* curr = Head; curr != NULL; curr = curr->next)
    printf("%d -> ", curr->data);
  printf("NULL\n");
}

void Error(const char* msg) {
  printf("File: %s, Line: %d, %s\n", __FILE__, __LINE__, msg);
  assert(false);
  exit(EXIT_FAILURE);
}

bool IsEmpty() { return !Head; }
void Create() { assert(!Head); }
void Destroy() {
  if (IsEmpty()) return;
  Node* prev = Head;
  for (Node* curr = Head->next; curr != NULL; curr = curr->next) {
    free(prev);
    prev = curr;
  }
  free(prev);
  Head = NULL;
}
int GetSize() {
  int size = 0;
  for (Node* curr = Head; curr != NULL; curr = curr->next) ++size;
  return size;
}
bool Find(int data) {
  Node* curr;
  for (curr = Head; curr != NULL && curr->data < data; curr = curr->next)
    ;
  return (curr != NULL && curr->data == data);
}
void Insert(int data) {
  Node* prev = NULL;
  Node* curr;
  for (curr = Head; curr != NULL && curr->data < data; curr = curr->next)
    prev = curr;
  Node* n = calloc(1, sizeof(Node));
  n->data = data;
  n->next = curr;
  if (prev == NULL)
    Head = n;
  else
    prev->next = n;
}
bool Remove(int data) {
  if (IsEmpty()) return false;
  if (GetSize() == 1 && Head->data == data) {
    Node* p = Head;
    Head = Head->next;
    free(p);
    return true;
  }
  Node* prev = NULL;
  Node* curr;
  for (curr = Head; curr != NULL && curr->data < data; curr = curr->next)
    prev = curr;
  if ((curr != NULL && curr->data == data) == false) return false;
  if (prev == NULL)
    Head = curr->next;
  else
    prev->next = curr->next;
  free(curr);
  return true;
}

void Test() {
  Create();
  Destroy();

  Create();
  assert(Remove(1) == false);
  Destroy();

  Create();
  Insert(1);
  assert(Find(1));
  assert(Remove(1));
  Destroy();

  Create();
  assert(Find(2) == false);
  Insert(2);
  assert(Remove(2));
  Destroy();

  Create();
  for (int i = 0; i < 100; ++i) Insert(i);
  for (int i = 0; i < 100; ++i) assert(Find(i));
  assert(GetSize() == 100);
  for (int i = 0; i < 100; ++i) assert(Remove(i));
  Destroy();
}

int main() {
  Test();
  return 0;
}
