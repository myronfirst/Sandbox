import random


def number_guessing_game():
    '''
    User gives lower and upper integer bounds
    CPU picks a random integer within the range
    The user tries to guess the number until he gets it right
    If guess is lower than number notify with message 'Too low'
    If guess is higher than number notify with message 'Too high'
    Terminate upon finding number
    Show number of guesses before terminating
    '''
    print('number guessing game')
    lower_bound = int(input('Set Lower Integer Bound: '))
    upper_bound = int(input('Set Upper Integer Bound: '))
    assert lower_bound < upper_bound
    random_choice = random.randint(lower_bound, upper_bound)
    tries = 0
    print('Guess the integer\n')
    while (True):
        player_choice = int(input('Integer Input: '))
        tries += 1
        if (player_choice < random_choice):
            print('Too low')
        elif (player_choice > random_choice):
            print('Too high')
        else:
            assert player_choice == random_choice
            print(
                f'Congratulation! You guessed the Integer! Integer is {random_choice}')
            break
    print(f'Tries: {tries}')


def hangman_game():
    '''
    User tries to guess a word from a given database
    Print each non revealed letter of randomly picked word as undescore
    Keep which letter positions are revealed in a list
    User guesses a number in a loop
    Print user tries in the end
    '''
    database = [
        'Myron',
        'Manos',
        'Olia',
        'Giorgos',
        'Eirini',
        'Dimitra',
        'Anastasia',
        'Maria',
        'Eleni',
        'Dionisis']
    database = [word.lower() for word in database]
    print('hangman game')
    word = random.choice(database)
    letter_revealed = [False for _ in word]
    letters_picked = ''
    while (True):
        for i, letter in enumerate(word):
            c = letter if letter_revealed[i] else '_'
            print(c, end=' ')
        print('')
        print(f'Letters Picked: {letters_picked}')
        if (all(letter_revealed)):
            print('Congratulation! You won hangman!')
            break
        user_letter = input('Pick Letter: ').lower()
        letters_picked += user_letter
        for i, letter in enumerate(word):
            if user_letter == letter:
                letter_revealed[i] = True
    print(f'Tries: {len(letters_picked)}')


def rock_paper_scissors_game():
    '''
    Game of rock paper scissors
    Map selection to integers: Rock(1) Paper(2) Scissors(3)
    Player picks selection, 'q' to quit
    CPU picks random selection
    Compare selections and print winner, repeat until player quits
    Upon quitting, print player and CPU scores
    '''
    print('rock paper scissors game')
    player_score = 0
    cpu_score = 0
    while (True):
        player_choice = input(
            'Pick Rock(1) - Paper(2) - Scissors(3) - Quit(q): ')
        if (player_choice == 'q'):
            print('Quitting...')
            break
        player_choice = int(player_choice)
        assert 1 <= player_choice <= 3
        cpu_choice = random.randint(1, 3)
        result = ''
        if (player_choice == cpu_choice):
            result = 'Tie'
        elif (player_choice == 1 and cpu_choice == 2):
            result = 'Paper beats Rock, CPU Wins'
            cpu_score += 1
        elif (player_choice == 1 and cpu_choice == 3):
            result = 'Rock beats Scissors, Player Wins'
            player_score += 1
        elif (player_choice == 2 and cpu_choice == 1):
            result = 'Paper beats Rock, Player Wins'
            player_score += 1
        elif (player_choice == 2 and cpu_choice == 3):
            result = 'Scissors beats Paper, CPU Wins'
            cpu_score += 1
        elif (player_choice == 3 and cpu_choice == 1):
            result = 'Rock beats Scissors, CPU Wins'
            cpu_score += 1
        elif (player_choice == 3 and cpu_choice == 2):
            result = 'Scissors beats Paper, Player Wins'
            player_score += 1
        else:
            assert False, 'How did we get here?'
        integer_to_word = ['Invalid', 'Rock', 'Paper', 'Scissors']
        print(f'Player choice: {integer_to_word[player_choice]}')
        print(f'CPU choice: {integer_to_word[cpu_choice]}')
        print(result)
    print(f'Player Score: {player_score}')
    print(f'CPU Score: {cpu_score}')


def factorial_iterative(num):
    assert num >= 0
    fact = 1
    while num > 0:
        fact *= num
        num -= 1
    return fact


def factorial_recursive(num):
    assert num >= 0
    if (num <= 1):
        return 1
    return num * factorial_recursive(num - 1)


def fibonacci_iterative(num):
    assert num >= 0
    if 0 <= num <= 1:
        return num
    previous = 0
    current = 1
    for _ in range(2, num + 1):
        sum = previous + current
        previous = current
        current = sum
    return current


def fibonacci_recursive(num):
    assert num >= 0
    if 0 <= num <= 1:
        return num
    return fibonacci_recursive(num - 1) + fibonacci_recursive(num - 2)


def fibonacci_iterative_memoized(num):
    assert num >= 0
    factorial_result_iterative = [0, 1]
    for i in range(2, num + 1):
        factorial_result_iterative.append(
            factorial_result_iterative[i - 1] + factorial_result_iterative[i - 2])
    return factorial_result_iterative[num]


fibonacci_result_recursive = []


def fibonacci_recursive_memoized(num):
    assert num >= 0
    global fibonacci_result_recursive
    if 0 <= num <= 1:
        return num
    if fibonacci_result_recursive[num] is None:
        fibonacci_result_recursive[num] = fibonacci_recursive_memoized(
            num - 1) + fibonacci_recursive_memoized(num - 2)
    return fibonacci_result_recursive[num]


def get_factorial(num):
    # return factorial_iterative(num)
    return factorial_recursive(num)


def get_fibonacci(num):
    # return fibonacci_iterative(num)
    return fibonacci_recursive(num)
    # return fibonacci_iterative_memoized(num)
    # global fibonacci_result_recursive
    # fibonacci_result_recursive = (num + 1) * [None]
    # return fibonacci_recursive_memoized(num)


def main():
    # number_guessing_game()
    # hangman_game()
    # rock_paper_scissors_game()
    n = 30
    # res = get_factorial(n)
    res = get_fibonacci(n)
    print(res)


if __name__ == '__main__':
    main()
