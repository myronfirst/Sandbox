# List Comprehension
# list constructor can take an iterable [ iterable ]
# list comprehension returns an iterable
# [ 3 modify element | 1 access list element | 2 optinally filter element]

# iterators are used to retrieve next values of objects without storing the whole sequence in memory -> saves memory
# generator expressions are the modern of expressing iterator functionality
# generator expression returns a generator iterator
# A generator function uses the yield keyword and a generator iterator.


def main():
    # list comprehension
    l = list(range(0, 10**4))
    new_l = [-i for i in l if i < 100 and i % 2 == 0]
    print(new_l)
    print([elem for elem in range(0, 100, 2)])

    # iterators
    range_it = iter(range(1, 5))
    print(type(range_it))
    print(next(range_it))

    # how to implement own iterators, the old way
    class MyLineIt:
        def __init__(self, stop_count):
            self.stop_count = stop_count

        def __iter__(self):
            self.line = ""
            return self

        def __next__(self):
            if len(self.line) >= self.stop_count:
                raise StopIteration
            self.line = self.line + "-"
            return self.line

    line_it = MyLineIt(3)  # constructor used to describe stop iteration conditions
    # print(type(line_it))
    line_it = iter(line_it)  # call iter method else it wont initialize the buffer
    print(type(line_it))
    print(next(line_it))
    print(next(line_it))
    print(next(line_it))
    # print(next(line_it))  # Raises StopIteration

    # how for loop works
    # for i in range(1, 10) is actually for i in iter(range(1, 10))
    # lets say y = iter(range(1, 10) happens only at loop initialzation
    # at the end of each iteration, next(y) is called

    # generators
    def generate_names_with(prefix):
        for name in ["Manos", "Mitsos", "Maria", "Meleni"]:
            yield prefix + name

    get_name_gen = generate_names_with("Cool ")
    print(type(generate_names_with))
    print(type(get_name_gen))
    print(next(get_name_gen))
    print(next(get_name_gen))
    print(next(get_name_gen))
    print(next(get_name_gen))
    # print(next(get_name))  # Raises StopIteration

    range_gen = (i for i in range(1, 5))
    print(type(range_gen))
    print(next(range_gen))
    print(next(range_gen))

    # sum with generator expresion
    s = sum(i for i in range(1, 10))
    print(s)


if __name__ == "__main__":
    main()
