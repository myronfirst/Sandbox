import urllib.request
import os

urlbase = 'http://hplgit.github.com/bioinf-py/data/'
yeast_file = 'yeast_chr1.txt'


def download_dna_file():
    if not os.path.isfile(yeast_file):
        url = urlbase + yeast_file
        urllib.request.urlretrieve(url, yeast_file)


def get_base_frequencies(dna):
    return {base: dna.count(base) / float(len(dna))
            for base in 'ATGC'}


def read_dna_file(filename):
    dna = ''
    for line in open(filename, 'r'):
        dna += line.strip()
    return dna


def main():
    download_dna_file()
    dna = read_dna_file(yeast_file)
    yeast_freq = get_base_frequencies(dna)
    print(f'Base frequencies of yeast DNA (length {len(dna)}):')
    print(yeast_freq)


if __name__ == "__main__":
    main()
