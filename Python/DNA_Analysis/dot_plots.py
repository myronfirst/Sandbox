import random
import numpy


def dotplot_list_of_lists(dna_x, dna_y):
    dotplot_matrix = [['0' for x in dna_x] for y in dna_y]
    for x_index, x_value in enumerate(dna_x):
        for y_index, y_value in enumerate(dna_y):
            if x_value == y_value:
                dotplot_matrix[y_index][x_index] = '1'
    return dotplot_matrix


def make_string(dotplot_matrix):
    rows = [' '.join(row) for row in dotplot_matrix]
    plot = '\n'.join(rows)
    # or combined
    plot = '\n'.join([' '.join(row) for row in dotplot_matrix])
    return plot


def make_string_expanded(dotplot_matrix):
    rows = []
    for row in dotplot_matrix:
        row_string = ' '.join(row)
        rows.append(row_string)
        plot = '\n'.join(rows)
    return plot


def dotplot_numpy(dna_x, dna_y):
    dotplot_matrix = numpy.zeros((len(dna_y), len(dna_x)), int)
    for x_index, x_value in enumerate(dna_x):
        for y_index, y_value in enumerate(dna_y):
            if x_value == y_value:
                dotplot_matrix[y_index, x_index] = 1
    return dotplot_matrix


def generate_string(N, alphabet='ACGT'):
    return ''.join([random.choice(alphabet) for i in range(N)])


def main():
    # N = 100
    # dna_x = list(generate_string(N))
    # dna_y = list(generate_string(N))
    dna_x = 'ACGT'
    dna_y = 'ACGT'
    dp_ll = dotplot_list_of_lists(dna_x, dna_y)
    dp_s = make_string(dp_ll)
    dp_se = make_string_expanded(dp_ll)
    dp_numpy = dotplot_numpy(dna_x, dna_y)
    print(dp_ll)
    print(dp_numpy)
    print(dp_s)
    print(dp_se)


if __name__ == "__main__":
    main()
