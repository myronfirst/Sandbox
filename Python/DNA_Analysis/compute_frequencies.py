import numpy
import random

# freq table for 'AGT', 'ACG', 'AAA'
#   |0|1|2|...|len-1
# A   3 1 1
# C   0 1 0
# G   0 1 1
# T   0 0 1


def freq_list_of_lists_v2(dna_list):
    frequency_matrix = [[0 for v in dna_list[0]] for x in 'ACGT']
    base2index = {'A': 0, 'C': 1, 'G': 2, 'T': 3}
    for dna in dna_list:
        for index, base in enumerate(dna):
            frequency_matrix[base2index[base]][index] += 1
    return frequency_matrix


def freq_numpy(dna_list):
    frequency_matrix = numpy.zeros((4, len(dna_list[0])), dtype=int)
    base2index = {'A': 0, 'C': 1, 'G': 2, 'T': 3}
    for dna in dna_list:
        for index, base in enumerate(dna):
            frequency_matrix[base2index[base]][index] += 1
    return frequency_matrix


def freq_dict_of_lists_v2(dna_list):
    n = max([len(dna) for dna in dna_list])
    frequency_matrix = {base: [0] * n for base in 'ACGT'}
    for dna in dna_list:
        for index, base in enumerate(dna):
            frequency_matrix[base][index] += 1
    return frequency_matrix


def freq_dict_of_dicts_v1(dna_list):
    n = max([len(dna) for dna in dna_list])
    frequency_matrix = {
        base: {
            index: 0 for index in range(n)} for base in 'ACGT'}
    for dna in dna_list:
        for index, base in enumerate(dna):
            frequency_matrix[base][index] += 1
    return frequency_matrix


# vectorized operation using numpy
def freq_dict_of_arrays_v2(dna_list):
    n = max([len(dna) for dna in dna_list])
    frequency_matrix = {base: numpy.zeros(n, dtype=int) for base in 'ACGT'}
    for dna in dna_list:
        dna = numpy.array(dna, dtype='c')
        for base in 'ACGT':
            frequency_matrix[base] += (dna == numpy.array(base, dtype='c'))
    return frequency_matrix


# freq matrix is list of lists
def find_consensus_v1(frequency_matrix):
    if isinstance(frequency_matrix, list) and \
            isinstance(frequency_matrix[0], list):
        pass  # right type
    else:
        raise TypeError('frequency_matrix must be list of lists')
    base2index = {'A': 0, 'C': 1, 'G': 2, 'T': 3}
    consensus = ''
    dna_length = len(frequency_matrix[0])
    for i in range(dna_length):  # loop over positions in string
        max_freq = -1  # holds the max freq. for this i
        max_freq_base = None  # holds the corresponding base
        for base in 'ACGT':
            if frequency_matrix[base2index[base]][i] > max_freq:
                max_freq = frequency_matrix[base2index[base]][i]
                max_freq_base = base
            elif frequency_matrix[base2index[base]][i] == max_freq:
                max_freq_base = '-'  # more than one base as max
        consensus += max_freq_base  # add new base with max freq
    return consensus


# freq matrix is dict of dicts
def find_consensus_v3(frequency_matrix):
    if isinstance(frequency_matrix, dict) and \
            isinstance(frequency_matrix['A'], dict):
        pass  # right type
    else:
        raise TypeError('frequency_matrix must be dict of dicts')
    consensus = ''
    dna_length = len(frequency_matrix['A'])
    for i in range(dna_length):  # loop over positions in string
        max_freq = -1  # holds the max freq. for this i
        max_freq_base = None  # holds the corresponding base
        for base in 'ACGT':
            if frequency_matrix[base][i] > max_freq:
                max_freq = frequency_matrix[base][i]
                max_freq_base = base
            elif frequency_matrix[base][i] == max_freq:
                max_freq_base = '-'  # more than one base as max
        consensus += max_freq_base  # add new base with max freq
    return consensus


def generate_string(N, alphabet='ACGT'):
    return ''.join([random.choice(alphabet) for i in range(N)])


def main():
    # N = 1000
    # dna_list = list(generate_string(N))
    dna_list = ['ACGT', 'AAAA', 'ACGA']
    freq_matrix_ll = freq_list_of_lists_v2(dna_list)
    freq_matrix_numpy = freq_numpy(dna_list)
    freq_matrix_dl = freq_dict_of_lists_v2(dna_list)
    freq_matrix_dd = freq_dict_of_dicts_v1(dna_list)
    freq_matrix_da = freq_dict_of_arrays_v2(dna_list)
    print(freq_matrix_ll)
    print(freq_matrix_numpy)
    print(freq_matrix_dl)
    print(freq_matrix_dd)
    print(freq_matrix_da)

    consensus_ll = find_consensus_v1(freq_matrix_ll)
    consensus_dd = find_consensus_v3(freq_matrix_dd)
    print(consensus_ll)
    print(consensus_dd)


if __name__ == "__main__":
    main()
