import random
import time
import itertools


# using counter, dna is list
def count_v1(dna, base):
    dna = list(dna)  # convert string to list of letters
    i = 0  # counter
    for c in dna:
        if c == base:
            i += 1
    return i


# using counter, dna is string
def count_v2(dna, base):
    i = 0  # counter
    for c in dna:
        if c == base:
            i += 1
    return i


# using for loop with index
def count_v3(dna, base):
    i = 0  # counter
    for j in range(len(dna)):
        if dna[j] == base:
            i += 1
    return i


# using while loop with index
def count_v4(dna, base):
    i = 0  # counter
    j = 0  # string index
    while j < len(dna):
        if dna[j] == base:
            i += 1
        j += 1
    return i


# boolean array of occurance at input position
def count_v5(dna, base):
    m = []  # matches for base in dna: m[i]=True if dna[i]==base
    for c in dna:
        if c == base:
            m.append(True)
        else:
            m.append(False)
    return sum(m)


# boolean array shorter conditional
def count_v6(dna, base):
    m = []  # matches for base in dna: m[i]=True if dna[i]==base
    for c in dna:
        m.append(True if c == base else False)
    return sum(m)


# boolean array even shorter conditional
def count_v7(dna, base):
    m = []  # matches for base in dna: m[i]=True if dna[i]==base
    for c in dna:
        m.append(c == base)
    return sum(m)


# boolean array list comprehension
def count_v8(dna, base):
    m = [c == base for c in dna]
    return sum(m)


# boolean array list comprehension shorter
def count_v9(dna, base):
    return sum([c == base for c in dna])


# boolean array generator
def count_v10(dna, base):
    return sum(c == base for c in dna)
    # m_g = (c == base for c in dna)
    # return sum(m_g)


# index array
def count_v11(dna, base):
    return len([i for i in range(len(dna)) if dna[i] == base])


# built-in count
def count_v12(dna, base):
    return dna.count(base)


def compare_efficiency(input_len, base):
    def generate_string(N, alphabet='ACGT'):
        return ''.join([random.choice(alphabet) for i in range(N)])
    dna = generate_string(input_len)
    functions = [count_v1, count_v2, count_v3, count_v4,
                 count_v5, count_v6, count_v7, count_v8,
                 count_v9, count_v10, count_v11, count_v12]
    timings = []  # timings[i] holds CPU time for functions[i]

    # time
    for function in functions:
        t0 = time.perf_counter()
        function(dna, base)
        t1 = time.perf_counter()
        cpu_time = t1 - t0
        timings.append(cpu_time)
    for (fun, timing) in zip(functions, timings):
        print(f'{fun.__name__} :\t{timing:.5f} secs')

    # correctness
    solution = dna.count(base)
    for f in functions:
        success = f(dna, base) == solution
        fail_msg = '%s failed' % f.__name__
        assert success, fail_msg


def main():
    N = 1000000
    base = 'A'
    compare_efficiency(N, base)


if __name__ == "__main__":
    main()
