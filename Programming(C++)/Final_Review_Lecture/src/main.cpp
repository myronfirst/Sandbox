#include <cassert>
#include <iostream>
#include <iterator>
#include <map>
#include <string>
#include <vector>

// ReverseString
void PrintReverseString(const std::string::reverse_iterator& rIt, const std::string& str) {
    if (rIt >= str.rend()) {
        std::cout << "\n";
        return;
    }
    std::cout << *rIt;
    return PrintReverseString(rIt + 1, str);
}
void ReverseString() {
    std::string str{};
    getline(std::cin, str);
    if (str.empty()) exit(EXIT_FAILURE);
    std::string::reverse_iterator rIt = str.rbegin();
    PrintReverseString(rIt, str);
}

// RationalClass
class Rational {
private:
    int nominator{ 0 };
    int denominator{ 1 };

public:
    Rational() = default;
    Rational(int _nominator, int _denominator) : nominator{ _nominator }, denominator{ _denominator } {
        if (denominator == 0) exit(EXIT_FAILURE);
    }
    Rational operator+(const Rational& other) const { return Rational{ nominator * other.denominator + other.nominator * denominator, denominator * other.denominator }; }
    Rational operator-(const Rational& other) const { return Rational{ nominator * other.denominator - other.nominator * denominator, denominator * other.denominator }; }
    Rational operator*(const Rational& other) const { return Rational{ nominator * other.nominator, denominator * other.denominator }; }
    Rational operator/(const Rational& other) const { return Rational{ nominator * other.denominator, denominator * other.nominator }; }
    double ToDouble() const { return nominator / (1.0 * denominator); }
};
void RationalTest() {
    Rational zero{};
    Rational a{ 1, 2 };
    Rational b{ 1, 3 };

    std::cout << (a + b).ToDouble() << "\n";
    std::cout << (a - b).ToDouble() << "\n";
    std::cout << (a * b).ToDouble() << "\n";
    std::cout << (a / b).ToDouble() << "\n";
}

int main() {
    // ReverseString();
    RationalTest();
    return 0;
}
