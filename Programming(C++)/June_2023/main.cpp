#include <fstream>
#include <iostream>
#include <memory>
#include <vector>

// Thema1

int GCD(int a, int b) {
  if (a == 0) return b;
  return GCD(b % a, a);
}
int Division(int a, int b) {
  if (a < b) return 0;
  return 1 + Division(a - b, b);
}
void Thema1() {
  std::cout << GCD(5, 6) << "\n";
  std::cout << Division(10, 2) << "\n";
}

// Thema2
void Swap(int& x, int& y) {
  int temp = x;
  x = y;
  y = temp;
}
void SelectionSort(int* arr, size_t sz) {
  for (size_t i = 0; i < sz; ++i) {
    int* min = &arr[i];
    for (size_t j = i; j < sz; ++j) {
      if (arr[j] < *min) min = &(arr[j]);
    }
    Swap(arr[i], *min);
  }
}
void Thema2() {
  int a[10] = {2, 4, 6, 8, 10, 9, 7, 5, 3, 1};
  SelectionSort(a, 10);
  for (size_t i = 0; i < 10; ++i) std::cout << a[i];
  std::cout << "\n";
}

// Thema3
void Thema3() {
  std::ifstream ifs{"in.txt"};
  if (!ifs.is_open()) {
    std::cout << "Cannot open input file\n";
    return exit(EXIT_FAILURE);
  }
  std::ofstream ofs{"out.txt"};
  if (!ofs.is_open()) {
    std::cout << "Cannot open output file\n";
    return exit(EXIT_FAILURE);
  }
  std::vector<std::string> words{};
  while (ifs.peek() != EOF) {
    std::string word;
    ifs >> word;
    words.emplace_back(word);
  }
  for (const auto& word : words) ofs << word << "\n";
}
// Thema4
class Animal {
 public:
  void virtual sound() const = 0;
  virtual ~Animal() = default;
};
class Lion : public Animal {
 public:
  void sound() const { std::cout << "roar\n"; }
  ~Lion() = default;
};
class Cat : public Animal {
 public:
  void sound() const { std::cout << "meow\n"; }
  ~Cat() = default;
};
void Thema4() {
  std::vector<Animal*> vec{};
  vec.emplace_back(new Lion());
  vec.emplace_back(new Cat());
  for (const auto& v : vec) v->sound();
  for (const auto& v : vec) delete v;
}

int main() {
  Thema1();
  Thema2();
  Thema3();
  Thema4();
}
