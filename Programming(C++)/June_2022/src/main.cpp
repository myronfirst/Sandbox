#include <cassert>
#include <iostream>
#include <map>
#include <vector>

// Thema1
void PrintVec(const std::vector<int>& vec) {
    for (int i : vec)
        std::cout << i;
    std::cout << "\n";
}
void Thema1() {
    std::vector<int> vec1(7);
    std::vector<int> vec2(10);
    for (size_t i = 0; i < 17; ++i) {
        int n;
        std::cin >> n;
        vec1.push_back(n);
        vec2.push_back(n);
    }
    PrintVec(vec1);
    PrintVec(vec2);
    std::vector vec3(vec2);
    std::cout << (vec3 == vec2) << "\n";
    std::cout << (vec3 != vec2) << "\n";
    // std::cout << (vec3 = vec2) << "\n";
    try {
        vec3.at(20);
    } catch (std::exception& e) {
        std::cout << "Out of Bounds Exception\n"
                  << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }
}
// Thema2
/* void Swap(int& x, int& y) {
    int temp = x;
    x = y;
    y = temp;
}
void SelectionSort(int* arr, size_t sz) {
    for (size_t i = 0; i < sz; ++i) {
        int* min = &arr[i];
        for (size_t j = i; j < sz; ++j) {
            if (arr[j] < *min)
                min = &(arr[j]);
        }
        Swap(arr[i], *min);
    }
} */
void SelectionSort(int* arr, size_t sz) {
    for (size_t i = 0; i < sz; ++i) {
        size_t minIdx = i;
        for (size_t j = i; j < sz; ++j) {
            if (arr[j] < arr[minIdx])
                minIdx = j;
        }
        int temp = arr[i];
        arr[i] = arr[minIdx];
        arr[minIdx] = temp;
    }
}
void Thema2() {
    int a[10] = { 2, 4, 6, 8, 10, 9, 7, 5, 3, 1 };
    SelectionSort(a, 10);
    for (size_t i = 0; i < 10; ++i)
        std::cout << a[i];
    std::cout << "\n";
}

// Thema3
void Thema3() {
    std::map<std::string, int> freq;
    while (true) {
        std::string word;
        std::cin >> word;
        if (word == "end") break;
        const auto& [it, inserted] = freq.insert({ word, 1 });
        if (!inserted) it->second += 1;
    }
    for (const auto& [word, count] : freq)
        std::cout << word << " : " << count << "\n";
}

// Thema4

// Vector3.h
class Vector3 {
private:
    double x{ 0.0 };
    double y{ 0.0 };
    double z{ 0.0 };

public:
    Vector3() = default;
    explicit Vector3(double _x, double _y, double _z) : x{ _x }, y{ _y }, z{ _z } {};
    explicit Vector3(double _x, double _y) : Vector3(_x, _y, 0.0){};
    explicit Vector3(double _x) : Vector3(_x, 0.0, 0.0){};
    friend std::ostream& operator<<(std::ostream& os, const Vector3& v);
    friend std::istream& operator>>(std::istream& is, Vector3& v);
    Vector3 operator+(const Vector3& v) const {
        Vector3 res{};
        res.x = x + v.x;
        res.y = y + v.y;
        res.z = z + v.z;
        return res;
    }
    Vector3 operator-(const Vector3& v) const {
        Vector3 res{};
        res.x = x - v.x;
        res.y = y - v.y;
        res.z = z - v.z;
        return res;
    }
    Vector3 operator*(float n) const {
        Vector3 res{};
        res.x = x * n;
        res.y = y * n;
        res.z = z * n;
        return res;
    }
    Vector3 operator/(float n) const {
        assert(n != 0);
        Vector3 res{};
        res.x = x / n;
        res.y = y / n;
        res.z = z / n;
        return res;
    }
    static double Dot(const Vector3& a, const Vector3& b) {
        return a.x * b.x + a.y * b.y + a.z * b.z;
    }
};
std::ostream& operator<<(std::ostream& os, const Vector3& v) {
    os << "(" << v.x << "," << v.y << "," << v.z << ")";
    return os;
}
std::istream& operator>>(std::istream& is, Vector3& v) {
    is >> v.x >> v.y >> v.z;
    return is;
}
class Ray {
private:
    Vector3 o{ 0.0 };
    Vector3 d{ 0.0 };
    float t{ 0.0 };

public:
    Ray() = default;
    Ray(Vector3 _o, Vector3 _d, float _t) : o{ _o }, d{ _d }, t{ _t } {};
    friend std::ostream& operator<<(std::ostream& os, const Ray& r);
};
std::ostream& operator<<(std::ostream& os, const Ray& r) {
    Vector3 res = r.o + r.d * r.t;
    os << res << " = " << r.o << "+" << r.t << "*" << r.d << "\n";
    return os;
}
void Thema4() {
    Vector3 a{};
    Vector3 b{ 1.0 };
    Vector3 c{ 1.0, 2.0 };
    Vector3 d{ 1.0, 2.0, 3.0 };

    Vector3 e = a + b;
    Vector3 f = c - d;
    std::cout << e * 2.0 << "\n";
    std::cout << f / 2.0 << "\n";
    std::cout << "Dot:" << (Vector3::Dot(Vector3{ 1.0, 0.0, 0.0 }, Vector3{ 0.0, 1.0, 0.0 })) << "\n";
    std::cout << "Ray:" << Ray(Vector3{ 0.0, 0.0, 0.0 }, Vector3{ 1.0, 0.0, 0.0 }, 2.0) << "\n";
}

void Thema5() {
    // a)
    constexpr size_t SIZE = 5;
    unsigned int values[SIZE] = { 2u, 4u, 6u, 8u, 10u };

    // b)
    unsigned int* vPtr;

    // c)
    for (size_t i = 0; i < SIZE; ++i)
        std::cout << " " << values[i];
    std::cout << "\n";

    // d)
    vPtr = values;
    vPtr = &(values[0]);

    // e)
    // ekfonisi address is 1002500 + 3 * 2 = 1002506, dereferenced value is 8
    unsigned int* addr = vPtr + 3;
    std::cout << addr << " : " << *addr;
    std::cout << "\n";
}

int main() {
    // Thema1();
    // Thema2();
    // Thema3();
    // Thema4();
    // Thema5();
    return 0;
}
